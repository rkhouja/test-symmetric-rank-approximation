using DynamicPolynomials, TensorDec

include("sym_tens_fct.jl")


function test_col(eps, alpha)
n=5
d=4
r=4
N=10
    d1=Float64[]
    d2=Float64[]
    t1s=Float64[]
    t2s=Float64[]
    N1=Float64[]

    for i in 1:N
        X = (@polyvar x[1:n])[1]
        w0 = fill(one(Complex{Float64}),r)
        V0 = zeros(ComplexF64,n,r)
        V0[:,1]=randn(ComplexF64,n)
              w=randn(ComplexF64,n)
              ws=w-V0[:,1]*(dot(w,V0[:,1])/(norm(V0[:,1]))^2)
              ws1=(ws/norm(ws))*alpha
              p=V0[:,1]+ws1
              V0[:,2]=p/norm(p)
              V0[:,1]=V0[:,1]/norm(V0[:,1])
              V0[:,3:end]=randn(ComplexF64,n,r-2)
              T0 = tensor(w0, V0, X, d)
               T   = T0 + rand_sym_tens_C(X,d)*eps
        t1 = @elapsed w1, V1, Info = decompose(T,r)
        T1 = tensor(w1, V1, X, d)
        err1=norm_apolar(T1-T0)
        push!(d1,err1/eps)
        push!(t1s,t1)
        t2 = @elapsed w2, V2, Info = RNS_TR(T, w1, V1)
        push!(N1,Info["nIter"])
        push!(t2s,t2)
        T2 = tensor(w2, V2, X, d)
        err2=norm_apolar(T2-T0)
        push!(d2,err2/eps)
    end

    d1_sort = sort(d1)
    min_dc=d1_sort[1]
    med_dc=d1_sort[div(N,2)]
    max_dc=d1_sort[end]

    d2_sort = sort(d2)
    min_rns = d2_sort[1]
    med_rns = d2_sort[div(N,2)]
    max_rns = d2_sort[end]

    return min_dc, med_dc, max_dc, min_rns, med_rns, max_rns, sum(t1s)/length(t1s), sum(t2s)/length(t2s), sum(N1)/length(N1)

end
