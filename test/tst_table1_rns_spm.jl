using DynamicPolynomials, TensorDec, MultivariateSeries

include("sym_tens_fct.jl")
include("spm.jl")
function tst_table1_rns_spm(n, d, r, eps, N=100)
    d1=Float64[]
    d2=Float64[]
    d3=Float64[]
    ts=Float64[]
    ts1=Float64[]
    N1=Float64[]
    N2=Float64[]

    for i in 1:N
        X = (@polyvar x[1:n])[1]
        w0 = fill(one(Float64),r)
        V0 = randn(Float64,n,r)
        T0 = tensor(w0, V0, X, d)
        T   = T0 + rand_sym_tens(X,d)*eps
        t1 = @elapsed w1, V1, Info = decompose(T,r)
        T1 = tensor(w1, V1, X, d)
        err1=norm_apolar(T1-T0)
        push!(d1,err1/eps)
        t2 = @elapsed w2, V2, Info = RNS_TR(T, w1, V1)
        push!(N1,Info["nIter"])
        push!(ts,t1+t2)
        T2 = tensor(w2, V2, X, d)
        err2=norm_apolar(T2-T0)
        push!(d2,err2/eps)

        v0 = randn(n);
        t3=@elapsed w3, V3, Info = spm_decompose(T, r, v0,
                                     Dict{String,Any}("maxIter" => 400,
                                                      "epsIter" => 1.e-10))
        push!(N2,sum(Info["nIter"]))
        push!(ts1,t3)
        T3 = tensor(w3, V3, X, d)
        err3=norm_apolar(T3-T0)
        push!(d3,err3/eps)
    end

    d1_sort = sort(d1)
    min_dc=d1_sort[1]
    med_dc=d1_sort[div(N,2)]
    max_dc=d1_sort[end]

    d2_sort = sort(d2)
    min_rns = d2_sort[1]
    med_rns = d2_sort[div(N,2)]
    max_rns = d2_sort[end]

    d3_sort = sort(d3)
    min_spm = d3_sort[1]
    med_spm = d3_sort[div(N,2)]
    max_spm = d3_sort[end]

    return min_rns, med_rns, max_rns, sum(ts)/length(ts), sum(N1)/length(N1), min_spm, med_spm, max_spm, sum(ts1)/length(ts1), sum(N2)/length(N2)

end
