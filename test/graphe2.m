% To call this function please enter: graphe1('path/variables2.mat')
%We save the 20 tensors from the following code:
%N=20;
%n=5;
%for s= 1:N
%A=rand(n,n,n,n,n,n);
%h(1:n,1:n,1:n,1:n,1:n,1:n)=-1;
%E0=h+A*2;
%E=zeros(n,n,n,n,n,n);
    %for p=perms(1:ndims(E0))'
     %E=E+permute(E0,p);
    %end
 %E=E/factorial(ndims(E0));
 %E=E/frob(E);
 %D(1:n,1:n,1:n,1:n,1:n,1:n,s)=E;
 %end
function [d2,t,Ni]=graphe2(variables2)
load(variables2);
L=load('variables2.mat','D');
T=L.D;
r=10;
n=5;
N=20;
M=10;
d1=zeros(M,N);
ts=zeros(M,1);
N1=zeros(M,1);
d2=zeros(3,N);
ts1=zeros(N,1);
N2=zeros(N,1);

for s= 1:N
    E=zeros(n,n,n,n,n,n)
    E=T(1:n,1:n,1:n,1:n,1:n,1:n,s);
    for k= 1:M   
    U2=cpd_rnd(n,r,@randn);
    U2=U2([1 1 1 1 1 1]);
    tic,[sol,output]=cpd_nls(E,U2),ts(k,1)=toc;
    N1(k,1)=output.iterations;
    z=sol{1};
    T3=zeros(n,n,n,n,n,n);
    for j= 1:r
        b=outprod(z(:,j),z(:,j),z(:,j),z(:,j),z(:,j),z(:,j));
        T3=T3+b;
    end
    d1(k,s)=frob(E-T3);
    end
a1=sort(d1(:,s));
d2(1,s)=a1(1,1);
d2(2,s)=sum(a1)/length(a1);
d2(3,s)=a1(10,1);
N2(s,1)=sum(N1)/length(N1);
ts1(s,1)=sum(ts)/length(ts);
end
d2
t=sum(ts1)/length(ts1)
Ni=sum(N2)/length(N2)
end

