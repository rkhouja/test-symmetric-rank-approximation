#FYI: The stopping criterion for the trust rigion's radius is by default 1.e-3.
#Nevertheless, for some examples of table 4 (in particular when the order of perturbation is less than 1.e-3 i.e. 1.e-4, 1.e-6);
#we choose this criterion to be 1.e-7 to get relative errors factors less than 1.

using DynamicPolynomials, TensorDec, MultivariateSeries
include("sym_tens_fct.jl")
function tst_table234_rns(n, d, r, eps, N, epsiter)
    #epsiter defines the minimal of the trust region's radius.
    d1=Float64[]
    d2=Float64[]
    t1s=Float64[]
    N1=Float64[]

    for i in 1:N
        X = (@polyvar x[1:n])[1]
        w0 = fill(one(Complex{Float64}),r)
        V0 = randn(Complex{Float64},n,r)
        T0 = tensor(w0, V0, X, d)
        T   = T0 + rand_sym_tens_C(X,d)*eps
        t1 = @elapsed w1, V1, Info = decompose(T,r)
        T1 = tensor(w1, V1, X, d)
        err1=norm_apolar(T1-T0)
        push!(d1,err1/eps)
        t2 = @elapsed w2, V2, Info = RNS_TR(T, w1, V1,Dict{String,Any}("maxIter" => 200,"epsIter" => epsiter))
        push!(N1,Info["nIter"])
        push!(t1s,t1+t2)
        T2 = tensor(w2, V2, X, d)
        err2=norm_apolar(T2-T0)
        push!(d2,err2/eps)
    end

    d1_sort = sort(d1)
    min_dc=d1_sort[1]
    med_dc=d1_sort[div(N,2)]
    max_dc=d1_sort[end]

    d2_sort = sort(d2)
    min_rns = d2_sort[1]
    med_rns = d2_sort[div(N,2)]
    max_rns = d2_sort[end]

    return min_rns, med_rns, max_rns, sum(t1s)/length(t1s), sum(N1)/length(N1)

end
