using DynamicPolynomials, TensorDec
function tst_ex44(r)
N=50
n=10
d=3
X = (@polyvar x[1:n])[1]
T = sum([(i^2)*X[i]^d for i in 1:n]) +
   sum([1.0*X[i]^(d-1) for i in 1:n])*sum([1.0*X[i] for i in 1:n])
    d1=Float64[]
    d2=Float64[]
    t1s=Float64[]
    N1=Float64[]

    for i in 1:N
         t1 = @elapsed w1, V1, Info = decompose(T,r)
         T1 = tensor(w1, V1, X, d)
         err1=norm_apolar(T1-T)
         push!(d1,err1)
         t2 = @elapsed w2, V2, Info = RNS_TR(T, w1, V1)
         push!(N1,Info["nIter"])
         push!(t1s,t1+t2)
         T2 = tensor(w2, V2, X, d)
         err2=norm_apolar(T2-T)
         push!(d2,err2)
    end

    d1_sort = sort(d1)
    min_dc=d1_sort[1]
    med_dc=d1_sort[div(N,2)]
    max_dc=d1_sort[end]

    d2_sort = sort(d2)
    min_rns = d2_sort[1]
    med_rns = d2_sort[div(N,2)]
    max_rns = d2_sort[end]

    return min_rns, med_rns, max_rns, sum(t1s)/length(t1s), sum(N1)/length(N1)

    end

    function tst_ex45(r)
    N=50
    n=10
    d=3
    X = (@polyvar x[1:n])[1]
    T = sum([exp(1.0*sqrt(i)+im*i^2)*(X[i])^d for i in 1:n]) +
        sum([(1.0*im*i/n)*X[i]^(d-1) for i in 1:n])*sum([X[i] for i in 1:n])
        d1=Float64[]
        d2=Float64[]
        t1s=Float64[]
        N1=Float64[]

        for i in 1:N
             t1 = @elapsed w1, V1, Info = decompose(T,r)
             T1 = tensor(w1, V1, X, d)
             err1=norm_apolar(T1-T)
             push!(d1,err1)
             t2 = @elapsed w2, V2, Info = RNS_TR(T, w1, V1)
             push!(N1,Info["nIter"])
             push!(t1s,t1+t2)
             T2 = tensor(w2, V2, X, d)
             err2=norm_apolar(T2-T)
             push!(d2,err2)
        end

        d1_sort = sort(d1)
        min_dc=d1_sort[1]
        med_dc=d1_sort[div(N,2)]
        max_dc=d1_sort[end]

        d2_sort = sort(d2)
        min_rns = d2_sort[1]
        med_rns = d2_sort[div(N,2)]
        max_rns = d2_sort[end]

        return min_rns, med_rns, max_rns, sum(t1s)/length(t1s), sum(N1)/length(N1)

        end
