using DynamicPolynomials, TensorDec, MultivariateSeries, MAT
include("spm.jl")
include("sym_tens_fct.jl")
function ahpi(T::Array,X)
    n=size(X,1)
    v=size(T)
    d=size(v,1)
    S=(sum(X[i] for i in 1:n))^d
    t=terms(S)
    s=size(t,1)
    t1=fill(0.0*X[1],s)
    for i in 1:s
        c=coefficient(t[i])
        m=monomial(t[i])
        a=fill(0.0,n)
        for j in 1:n
            a[j]=degree(m,X[j])
        end
         a = convert(Vector{Int64}, a)
        Ids=vcat([fill(k,a[k]) for k in 1:n]...)
        t1[i]=(c*T[Ids...])*m
    end
    P=sum(t1[i] for i in 1:s)

    return P
end

file=matopen("variables2.mat")
D=read(file,"D")
N=20
M=10
S=10
n=5
r=10
d=6
d1=zeros(10,20)
d11=zeros(10)
d2=zeros(3,20)
d3=zeros(10,20)
d4=zeros(3,20)
d33=zeros(10)
ts=zeros(10)
ts1=zeros(20)
ts2=zeros(10)
ts3=zeros(20)
N1=zeros(10)
N2=zeros(20)
N3=zeros(10)
N4=zeros(20)

    for i in 1:N
            T=zeros(n,n,n,n,n,n)
            X = (@polyvar x[1:n])[1]
            T=D[:,:,:,:,:,:,i]
            P=ahpi(T,X)
            for j in 1:M
                w1=fill(1,r)
                V1=randn(n,r)
                t2 = @elapsed w2, V2, Info = symr_iter(P, w1, V1)
                N1[j]=Info["nIter"]
                ts[j]=t2
                T2 = tensor(w2, V2, X, d)
                err1=norm_apolar(T2-P)
                d1[j,i]=err1
            end
            d11=sort(d1[:,i])
            d2[1,i]=d11[1]
            d2[2,i]=sum(d11)/length(d11)
            d2[3,i]=d11[10]
            N2[i]=sum(N1)/length(N1)
            ts1[i]=sum(ts)/length(ts)
            for s in 1:S
                v0 = randn(n);
                t3=@elapsed w3, V3, Info = spm_decompose(P, r, v0,
                                             Dict{String,Any}("maxIter" => 400,
                                                              "epsIter" => 1.e-4))
                N3[s]=sum(Info["nIter"])
                ts2[s]=t3
                T3 = tensor(w3, V3, X, d)
                err3=norm_apolar(T3-P)
                d3[s,i]=err3
                end
                d33=sort(d3[:,i])
                d4[1,i]=d33[1]
                d4[2,i]=sum(d33)/length(d33)
                d4[3,i]=d33[10]
                N4[i]=sum(N3)/length(N3)
                ts3[i]=sum(ts2)/length(ts2)

        end

        return d2, sum(ts1)/length(ts1), sum(N2)/length(N2), d4, sum(ts3)/length(ts3), sum(N4)/length(N4)
