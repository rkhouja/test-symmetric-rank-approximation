
## Test for low rank approximation of symmetric tensors
​
The codes in this test correspond to the experimentation performed in the paper:

*A Riemannian Newton optimization framework for the symmetric tensor rank approximation problem*,


  Rima Khouja, Houssam Khalil, and Bernard Mourrain

​
  https://hal.archives-ouvertes.fr/hal-02494172
  
​
To run the test in Julia, the latest version of the package `MultivariateSeries` and `TensorDec` should be installed:
​
```julia
] add https://github.com/bmourrain/MultivariateSeries.jl.git
```
​
```julia
] add https://gitlab.inria.fr/AlgebraicGeometricModeling/TensorDec.jl.git
```
